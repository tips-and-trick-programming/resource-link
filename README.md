RESOURCE CODING
---------------------------------
https://github.com/LaravelDaily/laravel-tips

https://github.com/Maatwebsite

https://github.com/rialto-php/puphpeteer

https://github.com/mehradsadeghi/laravel-crud-generator

https://github.com/laravel-shift/blueprint

https://github.com/datejs/Datejs

https://github.com/riktar/jkanban

https://github.com/puppeteer/puppeteer



https://apexcharts.com/

https://laravel-news.com/

https://wkhtmltopdf.org/

https://laravel-excel.com/

https://summernote.org/

https://quilljs.com/

https://dev.to/rickavmaniac/my-beloved-php-cheat-sheet-7dl

https://www.positronx.io/laravel-cron-job-task-scheduling-tutorial-with-example/

https://weareoutman.github.io/clockpicker/

https://kidsysco.github.io/jquery-ui-month-picker/

https://codepen.io/salahuddin/pen/LOmWpg



Flutter
-----------------------

https://github.com/icemanbsi/flutter_time_picker_spinner

https://github.com/JerinFrancisA/flutter_otp

https://github.com/fayeed/flutter_parsed_text

https://pub.dev/packages/blobs

https://github.com/Fliggy-Mobile/frefresh

https://github.com/Fliggy-Mobile/fdottedline

https://github.com/jogboms/time.dart



RESOURCE DESIGN
--------------------------------

https://absurd.design/

https://unsplash.com/

https://blush.design/

https://coverr.co

https://www.drawkit.io

https://www.humaaans.com

https://opendoodles.com

https://openpeeps.com

https://undraw.co/

https://elements.envato.com/free/powerpoint/infographic/

https://ipsm.io/

https://blobs.app

https://lokesh-coder.github.io/pretty-checkbox/

https://animista.net/

https://css.gg/

https://remixicon.com/

https://boxicons.com/

https://www.30secondsofcode.org/css/p/1

https://github.com/db1996/Pure-CSS-loaders

https://github.com/kristopolous/BOOTSTRA.386



DATA DUMMY
-----------------------

https://jsonplaceholder.typicode.com/

https://reqres.in/

https://placeimg.com/

https://www.fakedata.pro/



BROWSING SAAT GABUT
-----------------------------------------

https://www.producthunt.com/

https://graygrids.com/

https://github.com/explore



FREE STUFF
--------------------

https://witeboard.com

https://github.com/akiraux/Akira

https://anonfiles.com/

https://fontba.se/

https://freesound.org/

https://responsively.app/

https://standardresume.co

https://www.4kdownload.com/

https://superset.apache.org/

https://github.com/edwardsamuel/Wilayah-Administratif-Indonesia

https://echarts.apache.org/en/index.html

https://github.com/you-dont-need/You-Dont-Need-JavaScript



https://github.com/GitIndonesia/awesome-indonesia-repo

https://github.com/awesome-selfhosted/awesome-selfhosted

https://chrisnager.github.io/cursors/



LINUX APPS
--------------------

https://github.com/AdnanHodzic/auto-cpufreq

https://github.com/lakinduakash/linux-wifi-hotspot

https://github.com/LMMS/lmms

https://github.com/tuxedocomputers/tuxedo-control-center/

https://github.com/shiftkey/desktop

https://keepassxc.org/

https://ohmybash.nntoan.com/



COURSE
-----------------------

https://github.com/sindresorhus/awesome

https://www.30secondsofcode.org/

https://bradfrost.com/blog/post/atomic-web-design/

https://codemyui.com/

https://www.pluralsight.com/

https://www.productmanual.co

https://www.perforce.com/blog/alm/how-write-software-requirements-specification-srs-document



https://gitlab.com/tips-and-trick-programming/react-js

https://gitlab.com/tips-and-trick-programming/html-js-css

https://gitlab.com/tips-and-trick-programming/codeigniter-4

https://gitlab.com/tips-and-trick-programming/html-js-css



GRUP & CHANNEL TELEGRAM
--------------------------------------------------

https://t.me/codingmaster52

https://t.me/laravelnews

https://t.me/deadprogrammer

https://t.me/megaebookstore

https://t.me/courseudemygratis
